EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "voltmetre basse tension pas cher"
Date "2021-10-19"
Rev "v1.1"
Comp "apprentissage DAC & multiplexage"
Comment1 "license peu restrictive voir le fichier LICENSE du projet"
Comment2 "basé sur un ADC et le multiplexage d'afficheurs 7 segments"
Comment3 "https://gitlab.com/goblinrieur/voltmetre_num"
Comment4 "goblinrieur@gmail.com "
$EndDescr
$Comp
L PERSO:CA3161E U2
U 1 1 616F422F
P 5750 2800
F 0 "U2" H 5750 2865 50  0000 C CNN
F 1 "CA3161E" H 5750 2774 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_LongPads" H 5750 2800 50  0001 C CNN
F 3 "" H 5750 2800 50  0001 C CNN
	1    5750 2800
	1    0    0    -1  
$EndComp
$Comp
L PERSO:CA3162 U1
U 1 1 616F5454
P 4250 2500
F 0 "U1" H 3800 2600 50  0000 C CNN
F 1 "CA3162" H 3900 2500 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_LongPads" H 4050 2500 50  0001 C CNN
F 3 "" H 4050 2500 50  0001 C CNN
	1    4250 2500
	1    0    0    -1  
$EndComp
$Comp
L Adafruit:7805DT IC1
U 1 1 616F7D3C
P 4200 6500
F 0 "IC1" H 4200 6754 45  0000 C CNN
F 1 "AS7805DTR-E1" H 4200 6670 45  0000 C CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 4200 6500 50  0001 C CNN
F 3 "" H 4200 6500 50  0001 C CNN
	1    4200 6500
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J1
U 1 1 616F91A8
P 2050 6700
F 0 "J1" H 1942 6375 50  0000 C CNN
F 1 "9v bat block" H 1942 6466 50  0000 C CNN
F 2 "Connector_JST:JST_EH_B2B-EH-A_1x02_P2.50mm_Vertical" H 2050 6700 50  0001 C CNN
F 3 "~" H 2050 6700 50  0001 C CNN
	1    2050 6700
	-1   0    0    1   
$EndComp
$Comp
L Switch:SW_SPDT SW1
U 1 1 616FACF1
P 2750 6600
F 0 "SW1" H 2750 6275 50  0000 C CNN
F 1 "SW_SPDT" H 2750 6366 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x03_P2.54mm_Horizontal" H 2750 6600 50  0001 C CNN
F 3 "~" H 2750 6600 50  0001 C CNN
	1    2750 6600
	1    0    0    1   
$EndComp
NoConn ~ 2950 6700
$Comp
L Device:D D1
U 1 1 616FC974
P 3250 6500
F 0 "D1" H 3250 6283 50  0000 C CNN
F 1 "1n4004" H 3250 6374 50  0000 C CNN
F 2 "Diode_THT:D_A-405_P10.16mm_Horizontal" H 3250 6500 50  0001 C CNN
F 3 "~" H 3250 6500 50  0001 C CNN
	1    3250 6500
	-1   0    0    1   
$EndComp
$Comp
L Device:D D2
U 1 1 616FCFBE
P 4200 6100
F 0 "D2" H 4200 6317 50  0000 C CNN
F 1 "1n4004" H 4200 6226 50  0000 C CNN
F 2 "Diode_THT:D_A-405_P10.16mm_Horizontal" H 4200 6100 50  0001 C CNN
F 3 "~" H 4200 6100 50  0001 C CNN
	1    4200 6100
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 616FD69A
P 3600 6800
F 0 "C1" H 3715 6846 50  0000 L CNN
F 1 "330n" H 3715 6755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3638 6650 50  0001 C CNN
F 3 "~" H 3600 6800 50  0001 C CNN
	1    3600 6800
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 616FD9E7
P 4800 6800
F 0 "C2" H 4915 6846 50  0000 L CNN
F 1 "330n" H 4915 6755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4838 6650 50  0001 C CNN
F 3 "~" H 4800 6800 50  0001 C CNN
	1    4800 6800
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Polarized_US C3
U 1 1 616FEB04
P 5250 6800
F 0 "C3" H 5365 6846 50  0000 L CNN
F 1 "1µ" H 5365 6755 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_3x5.3" H 5250 6800 50  0001 C CNN
F 3 "~" H 5250 6800 50  0001 C CNN
	1    5250 6800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 616FF056
P 5250 7050
F 0 "#PWR05" H 5250 6800 50  0001 C CNN
F 1 "GND" H 5255 6877 50  0000 C CNN
F 2 "" H 5250 7050 50  0001 C CNN
F 3 "" H 5250 7050 50  0001 C CNN
	1    5250 7050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 616FF2F7
P 4800 7050
F 0 "#PWR04" H 4800 6800 50  0001 C CNN
F 1 "GND" H 4805 6877 50  0000 C CNN
F 2 "" H 4800 7050 50  0001 C CNN
F 3 "" H 4800 7050 50  0001 C CNN
	1    4800 7050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 616FF464
P 4200 7050
F 0 "#PWR03" H 4200 6800 50  0001 C CNN
F 1 "GND" H 4205 6877 50  0000 C CNN
F 2 "" H 4200 7050 50  0001 C CNN
F 3 "" H 4200 7050 50  0001 C CNN
	1    4200 7050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 616FF5CA
P 3600 7050
F 0 "#PWR02" H 3600 6800 50  0001 C CNN
F 1 "GND" H 3605 6877 50  0000 C CNN
F 2 "" H 3600 7050 50  0001 C CNN
F 3 "" H 3600 7050 50  0001 C CNN
	1    3600 7050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 616FF750
P 2550 7050
F 0 "#PWR01" H 2550 6800 50  0001 C CNN
F 1 "GND" H 2555 6877 50  0000 C CNN
F 2 "" H 2550 7050 50  0001 C CNN
F 3 "" H 2550 7050 50  0001 C CNN
	1    2550 7050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 6950 5250 7050
Wire Wire Line
	4800 6950 4800 7050
Wire Wire Line
	4200 7050 4200 6800
Wire Wire Line
	3600 6950 3600 7050
Wire Wire Line
	2550 7050 2550 6900
Wire Wire Line
	2550 6700 2250 6700
Wire Wire Line
	2250 6600 2550 6600
Wire Wire Line
	2950 6500 3100 6500
Wire Wire Line
	3400 6500 3600 6500
Wire Wire Line
	3600 6500 3600 6650
Connection ~ 3600 6500
Wire Wire Line
	3600 6500 3700 6500
Wire Wire Line
	3700 6500 3700 6100
Wire Wire Line
	3700 6100 4050 6100
Connection ~ 3700 6500
Wire Wire Line
	3700 6500 3800 6500
Wire Wire Line
	5250 6500 5250 6650
Wire Wire Line
	4600 6500 4700 6500
Wire Wire Line
	4800 6650 4800 6500
Connection ~ 4800 6500
Wire Wire Line
	4800 6500 5250 6500
Wire Wire Line
	4700 6500 4700 6100
Wire Wire Line
	4700 6100 4350 6100
Connection ~ 4700 6500
Wire Wire Line
	4700 6500 4800 6500
$Comp
L Display_Character:HDSP-A401 U3
U 1 1 617044D6
P 7250 3400
F 0 "U3" H 7250 4067 50  0000 C CNN
F 1 "HDSP-A401" H 7250 3976 50  0000 C CNN
F 2 "Display_7Segment:HDSP-A401" H 7250 2850 50  0001 C CNN
F 3 "https://docs.broadcom.com/docs/AV02-2553EN" H 6450 4400 50  0001 C CNN
	1    7250 3400
	1    0    0    -1  
$EndComp
$Comp
L Display_Character:HDSP-A401 U4
U 1 1 61704BCD
P 8250 3400
F 0 "U4" H 8250 4067 50  0000 C CNN
F 1 "HDSP-A401" H 8250 3976 50  0000 C CNN
F 2 "Display_7Segment:HDSP-A401" H 8250 2850 50  0001 C CNN
F 3 "https://docs.broadcom.com/docs/AV02-2553EN" H 7450 4400 50  0001 C CNN
	1    8250 3400
	1    0    0    -1  
$EndComp
$Comp
L Display_Character:HDSP-A401 U5
U 1 1 617051B9
P 9450 3400
F 0 "U5" H 9450 4067 50  0000 C CNN
F 1 "HDSP-A401" H 9450 3976 50  0000 C CNN
F 2 "Display_7Segment:HDSP-A401" H 9450 2850 50  0001 C CNN
F 3 "https://docs.broadcom.com/docs/AV02-2553EN" H 8650 4400 50  0001 C CNN
	1    9450 3400
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Potentiometer_US RV1
U 1 1 61706055
P 3000 2750
F 0 "RV1" H 2932 2704 50  0000 R CNN
F 1 "50k" H 2932 2795 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Bourns_3266W_Vertical" H 3000 2750 50  0001 C CNN
F 3 "~" H 3000 2750 50  0001 C CNN
	1    3000 2750
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Potentiometer_US RV2
U 1 1 61706AC3
P 4200 3900
F 0 "RV2" H 4500 3800 50  0000 R CNN
F 1 "10k" H 4500 4000 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Bourns_3266W_Vertical" H 4200 3900 50  0001 C CNN
F 3 "~" H 4200 3900 50  0001 C CNN
	1    4200 3900
	-1   0    0    1   
$EndComp
Text GLabel 5500 6500 2    50   Input ~ 0
+5v
Wire Wire Line
	5500 6500 5400 6500
Connection ~ 5250 6500
Connection ~ 5400 6500
Wire Wire Line
	5400 6500 5250 6500
Wire Wire Line
	3550 2700 3300 2700
Wire Wire Line
	3300 2700 3300 2550
Wire Wire Line
	3300 2550 3000 2550
Wire Wire Line
	3000 2550 3000 2600
Wire Wire Line
	3000 2900 3000 2950
Wire Wire Line
	3000 2950 3300 2950
Wire Wire Line
	3300 2950 3300 2800
Wire Wire Line
	3300 2800 3550 2800
NoConn ~ 3550 2900
Text Notes 1850 6850 0    50   ~ 0
12-9v continu
$Comp
L Device:R_US R1
U 1 1 6170D153
P 2350 2850
F 0 "R1" H 2418 2896 50  0000 L CNN
F 1 "33k" H 2418 2805 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2390 2840 50  0001 C CNN
F 3 "~" H 2350 2850 50  0001 C CNN
	1    2350 2850
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R2
U 1 1 6170DF31
P 2350 3350
F 0 "R2" H 2418 3396 50  0000 L CNN
F 1 "330" H 2418 3305 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2390 3340 50  0001 C CNN
F 3 "~" H 2350 3350 50  0001 C CNN
	1    2350 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 3100 2350 3100
Wire Wire Line
	2350 3100 2350 3000
Wire Wire Line
	2350 3200 2350 3100
Connection ~ 2350 3100
Wire Wire Line
	2350 2550 2350 2700
$Comp
L power:GND #PWR07
U 1 1 61712199
P 2350 3650
F 0 "#PWR07" H 2350 3400 50  0001 C CNN
F 1 "GND" H 2355 3477 50  0000 C CNN
F 2 "" H 2350 3650 50  0001 C CNN
F 3 "" H 2350 3650 50  0001 C CNN
	1    2350 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2350 3500 2350 3600
$Comp
L power:GND #PWR08
U 1 1 61712B2C
P 3300 3600
F 0 "#PWR08" H 3300 3350 50  0001 C CNN
F 1 "GND" H 3305 3427 50  0000 C CNN
F 2 "" H 3300 3600 50  0001 C CNN
F 3 "" H 3300 3600 50  0001 C CNN
	1    3300 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 3200 3300 3200
Wire Wire Line
	3300 3200 3300 3600
$Comp
L Connector:Conn_01x02_Female J2
U 1 1 61714817
P 1650 3150
F 0 "J2" H 1542 2825 50  0000 C CNN
F 1 "Sondes" H 1542 2916 50  0000 C CNN
F 2 "Connector_JST:JST_EH_B2B-EH-A_1x02_P2.50mm_Vertical" H 1650 3150 50  0001 C CNN
F 3 "~" H 1650 3150 50  0001 C CNN
	1    1650 3150
	-1   0    0    1   
$EndComp
Wire Wire Line
	2050 2550 2050 3050
Wire Wire Line
	2050 3050 1850 3050
Wire Wire Line
	2050 2550 2350 2550
Wire Wire Line
	1850 3150 2050 3150
Wire Wire Line
	2050 3150 2050 3600
Wire Wire Line
	2050 3600 2350 3600
Connection ~ 2350 3600
Wire Wire Line
	2350 3600 2350 3650
Text Notes 1550 3200 0    50   ~ 0
+\n\n-
Text GLabel 2550 2300 0    50   Input ~ 0
+5v
Wire Wire Line
	2550 2300 2700 2300
Wire Wire Line
	2700 2300 2700 2750
Wire Wire Line
	2700 2750 2850 2750
$Comp
L Device:C C4
U 1 1 6171F399
P 4200 2150
F 0 "C4" H 3900 2200 50  0000 L CNN
F 1 "330n" H 3900 2100 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4238 2000 50  0001 C CNN
F 3 "~" H 4200 2150 50  0001 C CNN
	1    4200 2150
	1    0    0    -1  
$EndComp
Text GLabel 3900 1700 0    50   Input ~ 0
+5v
Wire Wire Line
	4200 1700 3900 1700
Wire Wire Line
	4200 2300 4200 2400
Text Notes 3350 2050 0    50   ~ 0
faible absorbion \ndielectrique\npolystyrene par ex.
Wire Wire Line
	4200 1700 4200 2000
Text GLabel 4600 1700 2    50   Input ~ 0
+5v
Wire Wire Line
	4600 1700 4300 1700
Wire Wire Line
	4300 1700 4300 2400
$Comp
L power:GND #PWR09
U 1 1 6172BD50
P 4200 4250
F 0 "#PWR09" H 4200 4000 50  0001 C CNN
F 1 "GND" H 4205 4077 50  0000 C CNN
F 2 "" H 4200 4250 50  0001 C CNN
F 3 "" H 4200 4250 50  0001 C CNN
	1    4200 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 3750 4200 3600
Wire Wire Line
	4200 4050 4200 4150
Wire Wire Line
	4200 4150 3850 4150
Wire Wire Line
	3850 4150 3850 3900
Wire Wire Line
	3850 3900 4050 3900
Connection ~ 4200 4150
Wire Wire Line
	4200 4150 4200 4250
$Comp
L power:GND #PWR010
U 1 1 61730A38
P 4300 3650
F 0 "#PWR010" H 4300 3400 50  0001 C CNN
F 1 "GND" H 4305 3477 50  0000 C CNN
F 2 "" H 4300 3650 50  0001 C CNN
F 3 "" H 4300 3650 50  0001 C CNN
	1    4300 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 3600 4300 3650
Wire Wire Line
	5200 3000 4950 3000
Wire Wire Line
	4950 3100 5200 3100
Wire Wire Line
	5200 3200 4950 3200
Wire Wire Line
	4950 3300 5200 3300
Text GLabel 5050 3700 0    50   Input ~ 0
+5v
$Comp
L power:GND #PWR011
U 1 1 6173B5DE
P 5050 4250
F 0 "#PWR011" H 5050 4000 50  0001 C CNN
F 1 "GND" H 5055 4077 50  0000 C CNN
F 2 "" H 5050 4250 50  0001 C CNN
F 3 "" H 5050 4250 50  0001 C CNN
	1    5050 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 4250 5050 3800
Wire Wire Line
	5050 3800 5200 3800
Wire Wire Line
	5050 3700 5200 3700
Entry Wire Line
	6350 3100 6450 3200
Entry Wire Line
	6350 3200 6450 3300
Entry Wire Line
	6350 3300 6450 3400
Entry Wire Line
	6350 3400 6450 3500
Entry Wire Line
	6350 3500 6450 3600
Entry Wire Line
	6350 3600 6450 3700
Entry Wire Line
	6350 3700 6450 3800
Entry Wire Line
	6800 3000 6900 3100
Entry Wire Line
	6800 3100 6900 3200
Entry Wire Line
	6800 3200 6900 3300
Entry Wire Line
	6800 3300 6900 3400
Entry Wire Line
	6800 3400 6900 3500
Entry Wire Line
	6800 3500 6900 3600
Entry Wire Line
	6800 3600 6900 3700
Entry Wire Line
	7800 3000 7900 3100
Entry Wire Line
	7800 3100 7900 3200
Entry Wire Line
	7800 3200 7900 3300
Entry Wire Line
	7800 3300 7900 3400
Entry Wire Line
	7800 3400 7900 3500
Entry Wire Line
	7800 3500 7900 3600
Entry Wire Line
	7800 3600 7900 3700
Entry Wire Line
	9000 3000 9100 3100
Entry Wire Line
	9000 3100 9100 3200
Entry Wire Line
	9000 3200 9100 3300
Entry Wire Line
	9000 3300 9100 3400
Entry Wire Line
	9000 3400 9100 3500
Entry Wire Line
	9000 3500 9100 3600
Entry Wire Line
	9000 3600 9100 3700
$Comp
L Transistor_BJT:BC557 Q3
U 1 1 61702C49
P 9750 2500
F 0 "Q3" H 9941 2454 50  0000 L CNN
F 1 "BC557" H 9941 2545 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 9950 2425 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/BC556BTA-D.pdf" H 9750 2500 50  0001 L CNN
	1    9750 2500
	1    0    0    1   
$EndComp
$Comp
L Transistor_BJT:BC557 Q2
U 1 1 6170261D
P 8550 2500
F 0 "Q2" H 8741 2454 50  0000 L CNN
F 1 "BC557" H 8741 2545 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 8750 2425 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/BC556BTA-D.pdf" H 8550 2500 50  0001 L CNN
	1    8550 2500
	1    0    0    1   
$EndComp
$Comp
L Transistor_BJT:BC557 Q1
U 1 1 61701C78
P 7550 2500
F 0 "Q1" H 7741 2454 50  0000 L CNN
F 1 "BC557" H 7741 2545 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 7750 2425 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/BC556BTA-D.pdf" H 7550 2500 50  0001 L CNN
	1    7550 2500
	1    0    0    1   
$EndComp
Wire Wire Line
	7650 2700 7650 3700
Wire Wire Line
	7650 3700 7550 3700
Wire Wire Line
	8650 3700 8650 2700
Wire Wire Line
	9750 3700 9850 3700
Wire Wire Line
	9850 3700 9850 2700
NoConn ~ 9750 3800
NoConn ~ 8550 3800
NoConn ~ 7550 3800
NoConn ~ 9150 3800
NoConn ~ 6950 3800
$Comp
L Device:R_US R3
U 1 1 6178D359
P 7950 4050
F 0 "R3" H 8100 4150 50  0000 C CNN
F 1 "150" H 8100 4050 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7990 4040 50  0001 C CNN
F 3 "~" H 7950 4050 50  0001 C CNN
	1    7950 4050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR012
U 1 1 6178F2A0
P 7950 4300
F 0 "#PWR012" H 7950 4050 50  0001 C CNN
F 1 "GND" H 7955 4127 50  0000 C CNN
F 2 "" H 7950 4300 50  0001 C CNN
F 3 "" H 7950 4300 50  0001 C CNN
	1    7950 4300
	1    0    0    -1  
$EndComp
Connection ~ 6800 4900
Wire Bus Line
	6800 4900 6450 4900
Wire Wire Line
	7950 3800 7950 3900
Wire Wire Line
	7950 4200 7950 4300
Wire Bus Line
	6800 4900 7800 4900
Connection ~ 7800 4900
Wire Bus Line
	7800 4900 9000 4900
Wire Wire Line
	7900 3100 7950 3100
Wire Wire Line
	7950 3200 7900 3200
Wire Wire Line
	7900 3300 7950 3300
Wire Wire Line
	7950 3400 7900 3400
Wire Wire Line
	7900 3500 7950 3500
Wire Wire Line
	7950 3600 7900 3600
Wire Wire Line
	7900 3700 7950 3700
Entry Wire Line
	5050 2700 5150 2600
Entry Wire Line
	5050 2800 5150 2700
Entry Wire Line
	5050 2900 5150 2800
Entry Wire Line
	7150 2100 7250 2200
Entry Wire Line
	8150 2100 8250 2200
Entry Wire Line
	9350 2100 9450 2200
Wire Wire Line
	9450 2200 9450 2500
Wire Wire Line
	9450 2500 9550 2500
Wire Wire Line
	8250 2200 8250 2500
Wire Wire Line
	8250 2500 8350 2500
Wire Wire Line
	7350 2500 7250 2500
Wire Wire Line
	7250 2500 7250 2200
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 6180FB63
P 5400 6300
F 0 "#FLG0101" H 5400 6375 50  0001 C CNN
F 1 "PWR_FLAG" V 5400 6428 50  0000 L CNN
F 2 "" H 5400 6300 50  0001 C CNN
F 3 "~" H 5400 6300 50  0001 C CNN
	1    5400 6300
	0    1    1    0   
$EndComp
Wire Wire Line
	5400 6300 5400 6500
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 61810311
P 2550 6900
F 0 "#FLG0102" H 2550 6975 50  0001 C CNN
F 1 "PWR_FLAG" V 2550 7028 50  0000 L CNN
F 2 "" H 2550 6900 50  0001 C CNN
F 3 "~" H 2550 6900 50  0001 C CNN
	1    2550 6900
	0    1    1    0   
$EndComp
Connection ~ 2550 6900
Wire Wire Line
	2550 6900 2550 6700
Text GLabel 7700 2250 2    50   Input ~ 0
+5v
Text GLabel 8700 2250 2    50   Input ~ 0
+5v
Text GLabel 9900 2250 2    50   Input ~ 0
+5v
Wire Wire Line
	9900 2250 9850 2250
Wire Wire Line
	9850 2250 9850 2300
Wire Wire Line
	8700 2250 8650 2250
Wire Wire Line
	8650 2250 8650 2300
Wire Wire Line
	7700 2250 7650 2250
Wire Wire Line
	7650 2250 7650 2300
Wire Wire Line
	9150 3100 9100 3100
Wire Wire Line
	9100 3200 9150 3200
Wire Wire Line
	9100 3300 9150 3300
Wire Wire Line
	9100 3400 9150 3400
Wire Wire Line
	9100 3500 9150 3500
Wire Wire Line
	9150 3600 9100 3600
Wire Wire Line
	9150 3700 9100 3700
Text Label 9100 3100 0    50   ~ 0
a
Text Label 7900 3100 0    50   ~ 0
a
Text Label 6900 3100 0    50   ~ 0
a
Wire Wire Line
	6950 3100 6900 3100
Wire Wire Line
	6900 3200 6950 3200
Wire Wire Line
	6950 3300 6900 3300
Wire Wire Line
	6900 3400 6950 3400
Wire Wire Line
	6950 3500 6900 3500
Wire Wire Line
	6900 3600 6950 3600
Wire Wire Line
	6950 3700 6900 3700
Text Label 6350 3100 0    50   ~ 0
a
Text Label 6350 3200 0    50   ~ 0
b
Text Label 6900 3200 0    50   ~ 0
b
Text Label 7900 3200 0    50   ~ 0
b
Text Label 9100 3200 0    50   ~ 0
b
Text Label 9100 3300 0    50   ~ 0
c
Text Label 7900 3300 0    50   ~ 0
c
Text Label 6900 3300 0    50   ~ 0
c
Text Label 6350 3300 0    50   ~ 0
c
Text Label 9100 3400 0    50   ~ 0
d
Text Label 7900 3400 0    50   ~ 0
d
Text Label 6900 3400 0    50   ~ 0
d
Text Label 6350 3400 0    50   ~ 0
d
Text Label 6350 3500 0    50   ~ 0
e
Text Label 6900 3500 0    50   ~ 0
e
Text Label 6900 3500 0    50   ~ 0
e
Text Label 7900 3500 0    50   ~ 0
e
Text Label 9100 3500 0    50   ~ 0
e
Text Label 9100 3600 0    50   ~ 0
f
Text Label 7900 3600 0    50   ~ 0
f
Text Label 6900 3600 0    50   ~ 0
f
Text Label 6350 3600 0    50   ~ 0
f
Text Label 6350 3700 0    50   ~ 0
g
Text Label 6900 3700 0    50   ~ 0
g
Text Label 7900 3700 0    50   ~ 0
g
Text Label 9100 3700 0    50   ~ 0
g
Text Label 4950 2900 0    50   ~ 0
msd
Text Label 7250 2500 0    50   ~ 0
msd
Text Label 4950 2800 0    50   ~ 0
lsb
Text Label 8250 2500 0    50   ~ 0
lsb
Text Label 4950 2700 0    50   ~ 0
lsd
Text Label 9450 2500 0    50   ~ 0
lsd
Wire Wire Line
	6350 3100 6300 3100
Wire Wire Line
	6350 3200 6300 3200
Wire Wire Line
	6350 3300 6300 3300
Wire Wire Line
	6350 3400 6300 3400
Wire Wire Line
	6350 3500 6300 3500
Wire Wire Line
	6350 3600 6300 3600
Wire Wire Line
	6300 3700 6350 3700
Wire Wire Line
	8650 3700 8550 3700
Wire Wire Line
	5050 2700 4950 2700
Wire Wire Line
	4950 2800 5050 2800
Wire Wire Line
	5050 2900 4950 2900
Wire Bus Line
	5150 2100 9350 2100
Wire Bus Line
	5150 2100 5150 2800
Wire Bus Line
	6450 3200 6450 4900
Wire Bus Line
	6800 3000 6800 4900
Wire Bus Line
	7800 3000 7800 4900
Wire Bus Line
	9000 3000 9000 4900
$EndSCHEMATC
