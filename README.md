# voltmètre_num

Voltmètre base tension sans microcontroller.

# restrictions 

Faire un Voltmètre à affichage digital à afficheurs 7 segments.

Doit pouvoir fonctionner sur pile(s).

# fonctionnement de base 

Utiliser un CA3161 _convertisseur analogique/digital_, capable d'utiliser un mode rampe _(double rampe)_
; le CA3162 reçoit en mode BCD _broches 16-15-1-2_ pour sortir du 7segment en mode multiplexée.

Un ajustement permet de trouver le zéro volt & un autre le gain.

Le multiplexage pilote les 3 afficheurs 7 segments _HDSP-A401_ via 3 transistors _BC557_.

Pour faire très simple ce montage ne permet pas de varier les gammes d'entrées, donc la position du point _décimal_ est fixe.

Le schema initial est celui proposé par le datasheet du CA3162E.

# images PCB & schema 

![schema](./3.png)

![dessus](./1.png)

![dessous](./2.png)

[pdf schema](./prints/kicad_voltmetre.pdf)

[pdf PCB](./prints/PCB.pdf)

# datasheets

[1N4004](./datasheets/1N4004.pdf)

[AS7505](./datasheets/AS7805_TO-252-2.pdf)

[BC557](./datasheets/BC557.pdf)

[CA3161](./datasheets/CA3161_BCD_7SEG.pdf)

[CA3162](./datasheets/CA32162_ADC-BCD_3DIGITS.pdf)

[HDSP-A401](./datasheets/HDSP-A401.pdf)
